# jetose
2018，重新开始，以spring-boot为核心框架

## 模块架构
```
jetose
├── jetose-demo     -- 使用示例
     ├── druid-spring-boot-starter-demo     -- druid-spring-boot-starter使用示例
     ├── swagger-spring-boot-starter-demo     -- swagger-spring-boot-starter使用示例
├── jetose-starter     -- 集成插件
     ├── druid-spring-boot-starter     -- 集成druid - jdbc连接池、监控组件
     ├── swagger-spring-boot-starter     -- 集成swagger - restful风格的web服务框架
```