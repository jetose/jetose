# druid-spring-boot-starter-demo
druid-spring-boot-starter使用示例

## 访问地址
```
http://ip:port/druid
```

## 使用指南
- 引入jar包
```
<dependency>
    <groupId>com.gitee.jetose</groupId>
    <artifactId>druid-spring-boot-starter</artifactId>
    <version>${druid-spring-boot-starter.version}</version>
</dependency>
```
- 依赖jar包
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-jdbc</artifactId>
    <version>${spring-boot.version}</version>
</dependency>
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>${mysql-connector.version}</version>
</dependency>
```

## 配置说明
```
druid: 
    stat-view-servlet:      -- 统计信息
        enable: true     -- 是否启用
        urlPattern: /druid/*     -- 匹配规则
        allow: 127.0.0.1     -- 白名单
        deny: 192.168.1.102     -- 黑名单
        loginUsername: jetose     -- 登录用户
        loginPassword: 123456     -- 登录密码
        resetEnable: true     -- 是否允许清空统计数据
    web-stat-filter:     -- 监控信息
        enable: true     -- 是否启用
        urlPattern: /*     -- 匹配规则
        exclusions: "*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*"    -- 排除规则
```